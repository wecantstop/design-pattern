package com.xuzhiguang.study.designpattern.command;

public class CommandPatternDemo {

    public static void main(String[] args) {
        Command commandA = new CommandA();
        Command commandB = new CommandB();

        Invoker invoker = new Invoker();
        invoker.setCommand(commandA);
        invoker.invoke();

        invoker.setCommand(commandB);
        invoker.invoke();

    }
}
