package com.xuzhiguang.study.designpattern.command;

public class CommandA implements Command {
    public void execute() {
        System.out.println("执行命令A");
    }
}
