package com.xuzhiguang.study.designpattern.command;

public class Invoker {

    private Command command;

    public void invoke() {
        command.execute();
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }
}
