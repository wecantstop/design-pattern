package com.xuzhiguang.study.designpattern.command;

public interface Command {

    void execute();
}
