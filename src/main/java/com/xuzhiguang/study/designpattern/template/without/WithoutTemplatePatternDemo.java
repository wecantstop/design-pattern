package com.xuzhiguang.study.designpattern.template.without;

public class WithoutTemplatePatternDemo {


    public static void main(String[] args) {

        GivingGift1 givingGift1 = new GivingGift1();
        givingGift1.giving();

        GivingGift2 givingGift2 = new GivingGift2();
        givingGift2.giving();

        GivingGift3 givingGift3 = new GivingGift3();
        givingGift3.giving();

    }

}
