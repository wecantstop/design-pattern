package com.xuzhiguang.study.designpattern.template.with;

public class WithTemplatePatternDemo {

    public static void main(String[] args) {

        GivingGift givingGift1 = new GivingGift1();
        givingGift1.giving();

        GivingGift givingGift2 = new GivingGift2();
        givingGift2.giving();

        GivingGift givingGift3 = new GivingGift3();
        givingGift3.giving();


    }

}
