package com.xuzhiguang.study.designpattern.template.with;

public abstract class AbstractGivingGift implements GivingGift {

    protected abstract void customGiving();

    public void giving() {

        System.out.println("发放礼品前操作");
        customGiving();
        System.out.println("发放礼品后操作");

    }

}
