package com.xuzhiguang.study.designpattern.decorator;

public interface Component {

    void execute();

}
