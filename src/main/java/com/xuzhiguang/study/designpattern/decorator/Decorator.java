package com.xuzhiguang.study.designpattern.decorator;

public class Decorator implements Component {

    private Component component;

    public Decorator(Component component) {
        this.component = component;
    }

    public void execute() {
        System.out.println("执行基础功能之前逻辑");
        component.execute();
        System.out.println("执行基础功能之后逻辑");
    }
}
