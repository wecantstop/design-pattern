package com.xuzhiguang.study.designpattern.decorator;

public class ConcreteComponent implements Component{
    public void execute() {
        System.out.println("执行基础功能");
    }
}
