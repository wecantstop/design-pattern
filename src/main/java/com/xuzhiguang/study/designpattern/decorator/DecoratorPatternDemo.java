package com.xuzhiguang.study.designpattern.decorator;

public class DecoratorPatternDemo {

    public static void main(String[] args) {
        Component decorator = new Decorator(new ConcreteComponent());

        decorator.execute();
    }

}
