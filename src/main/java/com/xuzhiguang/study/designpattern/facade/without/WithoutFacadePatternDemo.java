package com.xuzhiguang.study.designpattern.facade.without;

public class WithoutFacadePatternDemo {


    public static void main(String[] args) {

        MemberService memberService = new MemberService();
        StampService stampService = new StampService();

        memberService.queryMember();
        stampService.gift();
    }

}
