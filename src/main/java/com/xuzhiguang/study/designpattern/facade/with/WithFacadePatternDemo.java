package com.xuzhiguang.study.designpattern.facade.with;

public class WithFacadePatternDemo {

    public static void main(String[] args) {
        MemberCenterService memberCenterService = new MemberCenterService();
        memberCenterService.gift();
    }

}
