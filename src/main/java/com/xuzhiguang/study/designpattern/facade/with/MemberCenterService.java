package com.xuzhiguang.study.designpattern.facade.with;


public class MemberCenterService {


    public void gift() {
        MemberService memberService = new MemberService();
        StampService stampService = new StampService();

        memberService.queryMember();
        stampService.gift();
    }
}
