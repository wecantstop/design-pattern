package com.xuzhiguang.study.designpattern.factory.with;

public class DefaultProduct implements Product {

    @Override
    public String toString() {
        return "我是一个默认的产品";
    }
}
