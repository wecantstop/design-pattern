package com.xuzhiguang.study.designpattern.factory.with;

public class WithFactoryPatternDemo {


    public static void main(String[] args) {
        Product product = ProductFactory.createProduct();

        System.out.println(product);


    }

}
