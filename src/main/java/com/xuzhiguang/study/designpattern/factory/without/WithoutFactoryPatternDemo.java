package com.xuzhiguang.study.designpattern.factory.without;

public class WithoutFactoryPatternDemo {

    public static void main(String[] args) {
        Product product = new Product();

        System.out.println(product);
    }

}
