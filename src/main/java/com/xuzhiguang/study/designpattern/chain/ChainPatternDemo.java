package com.xuzhiguang.study.designpattern.chain;

public class ChainPatternDemo {

    public static void main(String[] args) {

        Handler handlerA = new ConcreteHandlerA();

        Handler handlerB = new ConcreteHandlerB();

        Handler handlerC = new ConcreteHandlerC();

        handlerA.setSuccessor(handlerB);
        handlerB.setSuccessor(handlerC);

        int request = 10;

        handlerA.handleRequest(request);

        request = 29;

        handlerA.handleRequest(request);

    }

}
