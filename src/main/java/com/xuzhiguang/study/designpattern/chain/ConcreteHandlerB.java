package com.xuzhiguang.study.designpattern.chain;

public class ConcreteHandlerB extends Handler {


    public void handleRequest(int request) {
        if (request >= 10 && request < 20) {
            System.out.println("ConcreteHandlerB 处理请求：" + request);
        } else if(successor != null) {
            successor.handleRequest(request);
        }
    }
}
