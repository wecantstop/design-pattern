package com.xuzhiguang.study.designpattern.chain;

public class ConcreteHandlerA extends Handler {


    public void handleRequest(int request) {
        if (request >= 0 && request < 10) {
            System.out.println("ConcreteHandlerA 处理请求：" + request);
        } else if(successor != null) {
            successor.handleRequest(request);
        }
    }
}
