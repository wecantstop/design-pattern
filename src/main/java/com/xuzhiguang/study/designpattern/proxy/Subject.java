package com.xuzhiguang.study.designpattern.proxy;

public interface Subject {

    void request();

}
