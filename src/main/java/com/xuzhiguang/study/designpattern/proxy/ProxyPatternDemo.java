package com.xuzhiguang.study.designpattern.proxy;

public class ProxyPatternDemo {


    public static void main(String[] args) {

        Subject subject = new ConcreteSubject();
        Subject proxy = new Proxy(subject);

        proxy.request();


    }
}
