package com.xuzhiguang.study.designpattern.proxy;

public class Proxy implements Subject {

    private Subject subject;

    public Proxy(Subject subject) {
        this.subject = subject;
    }

    public void request() {
        System.out.println("执行额外的操作");
        subject.request();

    }
}
