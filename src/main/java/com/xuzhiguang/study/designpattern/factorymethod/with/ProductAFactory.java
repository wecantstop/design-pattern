package com.xuzhiguang.study.designpattern.factorymethod.with;


public class ProductAFactory extends AbstractProductFactory {
    protected Product create() {
        return new ProductA();
    }
}
