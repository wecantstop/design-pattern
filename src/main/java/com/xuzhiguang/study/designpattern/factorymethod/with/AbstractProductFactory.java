package com.xuzhiguang.study.designpattern.factorymethod.with;


public abstract class AbstractProductFactory implements ProductFactory {


    public void beforeProcess() {
        System.out.println("生产产品前操作");
        System.out.println("生产产品前操作2");

    }

    public Product createProduct() {

        beforeProcess();
        return create();
    }

    protected abstract Product create();

}
