package com.xuzhiguang.study.designpattern.factorymethod.without;


public class ProductAFactory {


    public static Product createProduct() {
        System.out.println("生产产品前操作");
        return new ProductA();
    }


}
