package com.xuzhiguang.study.designpattern.factorymethod.with;

public class ProductB implements Product {
    public String name() {
        return "产品B";
    }
}
