package com.xuzhiguang.study.designpattern.factorymethod.with;

public class WithFactoryMethodPatternDemo {

    public static void main(String[] args) {

        Product productA = new ProductAFactory().createProduct();
        Product productB = new ProductBFactory().createProduct();

        System.out.println(productA.name());

        System.out.println(productB.name());
    }

}
