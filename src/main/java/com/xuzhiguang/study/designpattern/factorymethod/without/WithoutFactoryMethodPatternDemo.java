package com.xuzhiguang.study.designpattern.factorymethod.without;

public class WithoutFactoryMethodPatternDemo {

    public static void main(String[] args) {

        Product productA = ProductAFactory.createProduct();
        Product productB = ProductBFactory.createProduct();

        System.out.println(productA.name());
        System.out.println(productB.name());

    }

}
