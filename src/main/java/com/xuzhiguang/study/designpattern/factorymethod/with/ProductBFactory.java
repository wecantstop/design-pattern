package com.xuzhiguang.study.designpattern.factorymethod.with;


public class ProductBFactory extends AbstractProductFactory {
    protected Product create() {
        return new ProductB();
    }
}
