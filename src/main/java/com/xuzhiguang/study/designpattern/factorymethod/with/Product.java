package com.xuzhiguang.study.designpattern.factorymethod.with;

public interface Product {


    String name();

}
