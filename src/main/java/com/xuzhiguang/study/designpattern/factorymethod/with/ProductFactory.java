package com.xuzhiguang.study.designpattern.factorymethod.with;


public interface ProductFactory {

    Product createProduct();

}
