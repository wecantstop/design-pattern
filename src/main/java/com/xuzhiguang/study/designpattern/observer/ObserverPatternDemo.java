package com.xuzhiguang.study.designpattern.observer;

public class ObserverPatternDemo {

    public static void main(String[] args) {
        Subject subject = new Subject(1);

        ConcreteObserver observer = new ConcreteObserver();
        subject.addObserver(observer);

        subject.changeState(2);

        subject.changeState(3);
    }
}
