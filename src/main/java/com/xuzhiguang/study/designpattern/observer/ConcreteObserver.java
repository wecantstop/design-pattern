package com.xuzhiguang.study.designpattern.observer;

import java.util.Observable;
import java.util.Observer;

public class ConcreteObserver implements Observer {


    public void update(Observable o, Object arg) {
        Subject subject = (Subject) o;

        System.out.println("对象发生了变化：" + subject);
    }
}
