package com.xuzhiguang.study.designpattern.observer;

import java.util.Observable;

public class Subject extends Observable {

    private Integer state;


    public Subject(Integer state) {
        this.state = state;
    }

    public void changeState(Integer state) {
        this.setState(state);
        this.setChanged();
        this.notifyObservers();

    }

    @Override
    public String toString() {
        return "Subject{" +
                "state=" + state +
                '}';
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

}
