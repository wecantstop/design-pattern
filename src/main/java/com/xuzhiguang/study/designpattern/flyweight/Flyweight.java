package com.xuzhiguang.study.designpattern.flyweight;

public interface Flyweight {

    void execute();

    String getName();

    void setName(String name);

}
