package com.xuzhiguang.study.designpattern.flyweight;

public class FlyweightPatternDemo {


    public static void main(String[] args) {
        Flyweight flyweight = FlyweightFactory.get("对象1");
        flyweight.execute();

        Flyweight flyweight1 = FlyweightFactory.get("对象1");
        flyweight1.execute();

        System.out.println(flyweight == flyweight1);
    }
}
