package com.xuzhiguang.study.designpattern.iterator.with;

import java.util.Arrays;

public class Cart implements Iterable<Goods> {

    private Integer listNo;

    private Boolean isVip;

    private Boolean isAuth;

    private Goods[] goodsList;


    public Iterator<Goods> iterator() {
        return new CartIterator(this);
    }

    public Cart() {

    }

    @Override
    public String toString() {
        return "Cart{" +
                "listNo=" + listNo +
                ", isVip=" + isVip +
                ", isAuth=" + isAuth +
                ", goodsList=" + Arrays.toString(goodsList) +
                '}';
    }

    public Integer getListNo() {
        return listNo;
    }

    public void setListNo(Integer listNo) {
        this.listNo = listNo;
    }

    public Boolean getVip() {
        return isVip;
    }

    public void setVip(Boolean vip) {
        isVip = vip;
    }

    public Boolean getAuth() {
        return isAuth;
    }

    public void setAuth(Boolean auth) {
        isAuth = auth;
    }

    public Goods[] getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(Goods[] goodsList) {
        this.goodsList = goodsList;
    }


}
