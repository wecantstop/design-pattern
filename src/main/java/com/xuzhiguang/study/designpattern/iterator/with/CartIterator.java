package com.xuzhiguang.study.designpattern.iterator.with;

public class CartIterator implements Iterator<Goods> {

    private Cart cart;

    private int index;

    public CartIterator(Cart cart) {
        this.cart = cart;
        this.index = 0;
    }

    public boolean hasNext() {

        return index < cart.getGoodsList().length;
    }

    public Goods next() {

        return cart.getGoodsList()[index++];
    }
}
