package com.xuzhiguang.study.designpattern.iterator.with;

/**
 * 迭代器模式 demo
 */
public class WithIteratorPatternDemo {


    public static Cart initCart() {
        Cart cart = new Cart();

        Goods goods1 = new Goods("6956553401099", "百事无糖可乐", 1);
        Goods goods2 = new Goods("6956553401098", "可口无糖可乐", 1);
        Goods goods3 = new Goods("6956553401097", "可口零度可乐", 1);

        Goods[] goodsList = {goods1, goods2, goods3};

        cart.setGoodsList(goodsList);

        return cart;
    }

    public static void main(String[] args) {

        Cart cart = initCart();

        Iterator<Goods> iterator = cart.iterator();

        while (iterator.hasNext()) {

            System.out.println(iterator.next());

        }

    }
    
}
