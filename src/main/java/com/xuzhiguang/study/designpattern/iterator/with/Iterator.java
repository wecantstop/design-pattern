package com.xuzhiguang.study.designpattern.iterator.with;


public interface Iterator<T> {

    boolean hasNext();


    T next();

}
