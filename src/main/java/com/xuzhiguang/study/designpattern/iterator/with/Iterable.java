package com.xuzhiguang.study.designpattern.iterator.with;

public interface Iterable<T> {


    Iterator<T> iterator();

}
