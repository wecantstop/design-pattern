package com.xuzhiguang.study.designpattern.iterator.with;

/**
 * 商品
 */
public class Goods {

    private String barcode;

    private String name;

    private Integer num;

    public Goods(String barcode, String name, Integer num) {
        this.barcode = barcode;
        this.name = name;
        this.num = num;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "barcode='" + barcode + '\'' +
                ", name='" + name + '\'' +
                ", num=" + num +
                '}';
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
