package com.xuzhiguang.study.designpattern.memento;

public class MementoPatternDemo {

    public static void main(String[] args) {

        Originator or = new Originator("hahaha");
        System.out.println(or.toString());


        Caretaker caretaker = new Caretaker();
        caretaker.setMemento(or.createMemento());

        or.setState("aaaaa");
        System.out.println(or.toString());


        or.restoreMemento(caretaker.getMemento());
        System.out.println(or.toString());
    }

}
