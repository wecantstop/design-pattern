package com.xuzhiguang.study.designpattern.mediator.with;

public class Mediator {

    private ColleagueA colleagueA;

    private ColleagueB colleagueB;

    public Mediator(ColleagueA colleagueA, ColleagueB colleagueB) {
        this.colleagueA = colleagueA;
        this.colleagueB = colleagueB;
    }

    public void executeA() {
        colleagueB.execute();
    }

    public void executeB() {
        colleagueA.execute();
    }

    public ColleagueA getColleagueA() {
        return colleagueA;
    }

    public void setColleagueA(ColleagueA colleagueA) {
        this.colleagueA = colleagueA;
    }

    public ColleagueB getColleagueB() {
        return colleagueB;
    }

    public void setColleagueB(ColleagueB colleagueB) {
        this.colleagueB = colleagueB;
    }

}
