package com.xuzhiguang.study.designpattern.mediator.with;

public class ColleagueB {


    public void execute(Mediator mediator) {

        mediator.executeB();
    }

    public void execute() {
        System.out.println("同事 B 被调用");
    }
}
