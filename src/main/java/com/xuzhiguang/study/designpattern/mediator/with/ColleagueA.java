package com.xuzhiguang.study.designpattern.mediator.with;

public class ColleagueA {


    public void execute(Mediator mediator) {

        mediator.executeA();
    }

    public void execute() {
        System.out.println("同事 A 被调用");
    }
}
