package com.xuzhiguang.study.designpattern.mediator.without;

public class ColleagueA {


    public void execute(ColleagueB colleague) {
        colleague.execute();
    }

    public void execute() {
        System.out.println("同事 A 被调用");
    }
}
