package com.xuzhiguang.study.designpattern.mediator.with;

public class WithMediatorPatternDemo {

    public static void main(String[] args) {

        ColleagueA colleagueA = new ColleagueA();
        ColleagueB colleagueB = new ColleagueB();
        Mediator mediator = new Mediator(colleagueA, colleagueB);

        colleagueA.execute(mediator);
        colleagueB.execute(mediator);


    }

}
