package com.xuzhiguang.study.designpattern.mediator.without;

public class WithoutMediatorPatternDemo {

    public static void main(String[] args) {


        ColleagueA colleagueA = new ColleagueA();
        ColleagueB colleagueB = new ColleagueB();

        colleagueA.execute(colleagueB);
        colleagueB.execute(colleagueA);

    }

}
