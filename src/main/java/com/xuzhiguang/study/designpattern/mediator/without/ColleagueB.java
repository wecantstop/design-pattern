package com.xuzhiguang.study.designpattern.mediator.without;

public class ColleagueB {


    public void execute(ColleagueA colleague) {

        colleague.execute();
    }

    public void execute() {
        System.out.println("同事 B 被调用");
    }
}
