package com.xuzhiguang.study.designpattern.visitor;

public class ConcreteElementA extends Element {
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
