package com.xuzhiguang.study.designpattern.visitor;

public class VisitorPatternDemo {

    public static void main(String[] args) {
        Element elementA = new ConcreteElementA();
        Element elementB = new ConcreteElementB();

        Visitor visitor = new ConcreteVisitor();

        elementA.accept(visitor);

        elementB.accept(visitor);
    }

}
