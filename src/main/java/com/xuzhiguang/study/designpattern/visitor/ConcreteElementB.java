package com.xuzhiguang.study.designpattern.visitor;

public class ConcreteElementB extends Element {
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
