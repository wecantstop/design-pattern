package com.xuzhiguang.study.designpattern.visitor;

public abstract class Visitor {


    abstract void visit(ConcreteElementA element);

    abstract void visit(ConcreteElementB element);
}
