package com.xuzhiguang.study.designpattern.visitor;

public class ConcreteVisitor extends Visitor {
    void visit(ConcreteElementA element) {
        System.out.println(element.getClass().getName() + "被" + this.getClass().getName() + "访问");
    }

    void visit(ConcreteElementB element) {
        System.out.println(element.getClass().getName() + "被" + this.getClass().getName() + "访问");
    }
}
