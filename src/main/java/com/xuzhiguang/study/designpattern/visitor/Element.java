package com.xuzhiguang.study.designpattern.visitor;

public abstract class Element {

    public abstract void accept(Visitor visitor);

}
