package com.xuzhiguang.study.designpattern.abstractfactory;

public class HighEndMask implements Mask {
    public String name() {
        return "高端口罩";
    }
}
