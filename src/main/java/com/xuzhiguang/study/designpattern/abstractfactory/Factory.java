package com.xuzhiguang.study.designpattern.abstractfactory;

public interface Factory {

    Mask createMask();

    ProtectiveSuit createProtectiveSuit();

}
