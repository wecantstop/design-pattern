package com.xuzhiguang.study.designpattern.abstractfactory;

public interface Mask {


    String name();

}
