package com.xuzhiguang.study.designpattern.abstractfactory;

public class HighEndFactory implements Factory {
    public Mask createMask() {
        return new HighEndMask();
    }

    public ProtectiveSuit createProtectiveSuit() {
        return new HighEndProtectiveSuit();
    }
}
