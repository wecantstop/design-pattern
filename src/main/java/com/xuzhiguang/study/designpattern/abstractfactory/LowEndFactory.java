package com.xuzhiguang.study.designpattern.abstractfactory;

public class LowEndFactory implements Factory {
    public Mask createMask() {
        return new LowEndMask();
    }

    public ProtectiveSuit createProtectiveSuit() {
        return new LowEndProtectiveSuit();
    }
}
