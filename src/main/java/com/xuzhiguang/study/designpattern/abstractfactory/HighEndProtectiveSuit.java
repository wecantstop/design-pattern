package com.xuzhiguang.study.designpattern.abstractfactory;

public class HighEndProtectiveSuit implements ProtectiveSuit {
    public String name() {
        return "高端防护服";
    }
}
