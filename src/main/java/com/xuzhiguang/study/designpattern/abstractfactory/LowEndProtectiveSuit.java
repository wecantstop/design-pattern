package com.xuzhiguang.study.designpattern.abstractfactory;

public class LowEndProtectiveSuit implements ProtectiveSuit {

    public String name() {
        return "低端防护服";
    }
}
