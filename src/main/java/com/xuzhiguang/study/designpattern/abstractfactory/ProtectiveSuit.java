package com.xuzhiguang.study.designpattern.abstractfactory;

public interface ProtectiveSuit {

    String name();
}
