package com.xuzhiguang.study.designpattern.abstractfactory;

public class LowEndMask implements Mask {
    public String name() {
        return "低端口罩";
    }
}
