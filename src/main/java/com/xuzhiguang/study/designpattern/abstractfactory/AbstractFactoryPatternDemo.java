package com.xuzhiguang.study.designpattern.abstractfactory;

public class AbstractFactoryPatternDemo {


    public static void main(String[] args) {
        Factory highEndFactory = new HighEndFactory();

        Mask highEndMask = highEndFactory.createMask();
        ProtectiveSuit highEndProtectiveSuit = highEndFactory.createProtectiveSuit();

        System.out.println(highEndMask.name());
        System.out.println(highEndProtectiveSuit.name());
    }

}
