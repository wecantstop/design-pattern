package com.xuzhiguang.study.designpattern.strategy.with;

public class PayFactory {

    public static Pay getPay(int payType) {

        switch (payType) {

            case 1:
                return new WxPay();
            case 2:
                return new AliPay();
            default:
                return null;

        }

    }


}
