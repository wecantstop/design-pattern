package com.xuzhiguang.study.designpattern.strategy.with;

public class Context {

    private Pay pay;

    public void pay() {
        pay.pay();
    }

    public Pay getPay() {
        return pay;
    }

    public void setPay(Pay pay) {
        this.pay = pay;
    }
}
