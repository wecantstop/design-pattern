package com.xuzhiguang.study.designpattern.strategy.with;

public class WithStrategyPatternDemo {

    public static void main(String[] args) {
        int payType = 1;

        Pay pay = PayFactory.getPay(payType);

        Context context = new Context();
        context.setPay(pay);

        context.pay();
    }

}
