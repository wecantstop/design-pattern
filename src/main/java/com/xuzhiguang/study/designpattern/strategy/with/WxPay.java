package com.xuzhiguang.study.designpattern.strategy.with;

public class WxPay implements Pay {
    public void pay() {
        System.out.println("微信支付");
    }
}
