package com.xuzhiguang.study.designpattern.strategy.without;

public class WithoutStrategyPatternDemo {

    public static void main(String[] args) {

        int payType = 1;

        if (payType == 1) {

            System.out.println("支付方式 1");
        } else if (payType == 2) {

            System.out.println("支付方式 2");
        } else if (payType == 3) {

            System.out.println("支付方式 3");
        } else {

            System.out.println("不支持的支付方式");
        }

    }
}
