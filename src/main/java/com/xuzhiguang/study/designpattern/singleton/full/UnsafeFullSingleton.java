package com.xuzhiguang.study.designpattern.singleton.full;

public class UnsafeFullSingleton {

    private static UnsafeFullSingleton unsafeFullSingleton;

    private UnsafeFullSingleton() {

    }

    public static UnsafeFullSingleton getInstance() {

        if (unsafeFullSingleton == null) {
            unsafeFullSingleton = new UnsafeFullSingleton();
        }
        return unsafeFullSingleton;
    }
}
