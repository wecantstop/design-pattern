package com.xuzhiguang.study.designpattern.singleton.full;

public class JVMSafeFullSingleton {


    private JVMSafeFullSingleton() {

    }

    private static class LazyHolder {
        public static final JVMSafeFullSingleton INSTANCE = new JVMSafeFullSingleton();
    }

    public static JVMSafeFullSingleton getInstance() {
        return LazyHolder.INSTANCE;
    }


}
