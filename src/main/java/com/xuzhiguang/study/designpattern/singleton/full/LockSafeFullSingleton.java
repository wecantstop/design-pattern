package com.xuzhiguang.study.designpattern.singleton.full;

public class LockSafeFullSingleton {

    private volatile static LockSafeFullSingleton instance;

    private LockSafeFullSingleton() {}

    public static LockSafeFullSingleton getInstance() {


        if (instance == null) {

            synchronized (LockSafeFullSingleton.class) {
                if (instance == null) {
                    instance = new LockSafeFullSingleton();
                }
            }
        }

        return instance;
    }


}
