package com.xuzhiguang.study.designpattern.singleton.full;

public class FullSingletonPatternDemo {

    public static void main(String[] args) {

        JVMSafeFullSingleton jvmSafeFullSingleton = JVMSafeFullSingleton.getInstance();

        System.out.println(jvmSafeFullSingleton);

    }

}
