package com.xuzhiguang.study.designpattern.prototype.with;

public class WithPrototypePatternDemo {

    public static void main(String[] args) {

        Product product = new Product("产品", new Component("组件"));

        try {
            Product product1 = (Product) product.clone();
            System.out.println(product1);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

    }

}
