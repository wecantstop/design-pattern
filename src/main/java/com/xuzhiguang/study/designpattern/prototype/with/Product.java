package com.xuzhiguang.study.designpattern.prototype.with;

public class Product {

    private String name;

    private Component component;

    public Product(String name, Component component) {
        this.name = name;
        this.component = component;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {

        return new Product(this.name, (Component) component.clone());
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", component=" + component +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Component getComponent() {
        return component;
    }

    public void setComponent(Component component) {
        this.component = component;
    }
}
