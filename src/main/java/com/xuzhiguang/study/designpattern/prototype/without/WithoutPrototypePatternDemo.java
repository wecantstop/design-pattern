package com.xuzhiguang.study.designpattern.prototype.without;

public class WithoutPrototypePatternDemo {

    public static void main(String[] args) {

        Product product = new Product("产品", new Component("组件"));

        Product product1 = new Product(product.getName(), new Component(product.getComponent().getName()));

        System.out.println(product1);

    }

}
