package com.xuzhiguang.study.designpattern.adapter.with;

public class InterfaceAdapter implements NewInterface {

    private final OldInterface oldInterface;

    public InterfaceAdapter(OldInterface oldInterface) {
        this.oldInterface = oldInterface;
    }

    public void newMethod() {

        System.out.println("通过适配器调用新接口");

        oldInterface.oldMethod();
    }
}
