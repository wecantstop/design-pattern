package com.xuzhiguang.study.designpattern.adapter.with;

public class WithAdapterPatternDemo {


    public static void main(String[] args) {

        OldInterface oldInterface = new OldInterfaceImpl();
        oldInterface.oldMethod();

        NewInterface newInterface = new InterfaceAdapter(new OldInterfaceImpl());
        newInterface.newMethod();

    }

}
