package com.xuzhiguang.study.designpattern.adapter.without;

public class WithoutAdapterPatternDemo {

    public static void main(String[] args) {

        OldInterface oldInterface = new OldInterfaceImpl();
        oldInterface.oldMethod();


        NewInterface newInterface = new NewInterfaceImpl();
        newInterface.newMethod();

    }

}
