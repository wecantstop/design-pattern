package com.xuzhiguang.study.designpattern.bridge;

public abstract class Handset {

    protected HandsetSoft handsetSoft;


    public abstract void run();

    public HandsetSoft getHandsetSoft() {
        return handsetSoft;
    }

    public void setHandsetSoft(HandsetSoft handsetSoft) {
        this.handsetSoft = handsetSoft;
    }
}
