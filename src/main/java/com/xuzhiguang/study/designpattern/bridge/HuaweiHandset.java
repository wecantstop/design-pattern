package com.xuzhiguang.study.designpattern.bridge;

public class HuaweiHandset extends Handset {
    public void run() {
        this.handsetSoft.run();
    }
}
