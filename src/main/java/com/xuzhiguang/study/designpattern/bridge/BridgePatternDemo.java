package com.xuzhiguang.study.designpattern.bridge;

public class BridgePatternDemo {

    public static void main(String[] args) {
        Handset xiaomi = new XiaomiHandset();
        xiaomi.setHandsetSoft(new HandsetGame());
        xiaomi.run();

        xiaomi.setHandsetSoft(new HandsetBook());
        xiaomi.run();
    }

}
