package com.xuzhiguang.study.designpattern.bridge;

public class XiaomiHandset extends Handset {
    public void run() {
        this.handsetSoft.run();
    }
}
