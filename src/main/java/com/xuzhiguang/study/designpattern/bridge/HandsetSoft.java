package com.xuzhiguang.study.designpattern.bridge;

public interface HandsetSoft {

    void run();
}
