package com.xuzhiguang.study.designpattern.composite;

import java.util.ArrayList;
import java.util.List;

public class Composite extends Component {

    private List<Component> child = new ArrayList<Component>();

    protected Composite(String name) {
        super(name);
    }

    void add(Component c) {
        child.add(c);
    }

    void remove(Component c) {
        child.remove(c);
    }

    void display() {

        System.out.println(this.toString());

        for (Component component : child) {
            component.display();
        }
    }

}
