package com.xuzhiguang.study.designpattern.composite;

public class CompositePatternDemo {

    public static void main(String[] args) {

        Component component = new Composite("根节点");

        Component component1 = new Composite("父节点1");
        component.add(component1);

        Component component2 = new Composite("父节点2");
        component.add(component2);

        Component component3 = new Composite("父节点3");
        component.add(component3);

        Component component4 = new Leaf("叶子结点4");
        component1.add(component4);

        Component component5 = new Leaf("叶子结点5");
        component1.add(component5);

        Component component6 = new Leaf("叶子结点6");
        component1.add(component6);

        Component component7 = new Leaf("叶子结点7");
        component2.add(component7);


        component.display();

        component.remove(component1);

        System.out.println("------");

        component.display();



    }
}
