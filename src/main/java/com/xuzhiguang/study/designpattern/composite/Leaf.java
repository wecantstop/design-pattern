package com.xuzhiguang.study.designpattern.composite;

public class Leaf extends Component {


    protected Leaf(String name) {
        super(name);
    }

    void add(Component c) {
        System.out.println("不支持");
    }

    void remove(Component c) {
        System.out.println("不支持");
    }

    void display() {
        System.out.println(this.toString());
    }
}
