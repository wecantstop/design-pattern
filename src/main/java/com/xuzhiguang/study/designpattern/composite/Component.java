package com.xuzhiguang.study.designpattern.composite;

public abstract class Component {

    private String name;

    protected Component(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Component{" +
                "name='" + name + '\'' +
                '}';
    }

    abstract void add(Component c);

    abstract void remove(Component c);

    abstract void display();

}
