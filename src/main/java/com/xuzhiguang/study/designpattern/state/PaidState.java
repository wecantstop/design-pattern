package com.xuzhiguang.study.designpattern.state;

public class PaidState implements State {
    public void execute() {
        System.out.println("执行支付完成逻辑");
    }
}
