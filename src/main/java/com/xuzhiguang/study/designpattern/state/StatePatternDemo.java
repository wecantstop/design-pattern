package com.xuzhiguang.study.designpattern.state;

public class StatePatternDemo {

    public static void main(String[] args) {
        State state = new NotPayState();

        Context context = new Context(state);

        context.execute(2);
        context.execute(3);
    }

}
