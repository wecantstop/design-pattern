package com.xuzhiguang.study.designpattern.state;

public class Context {

    private State state;

    public Context(State state) {
        this.state = state;
        this.state.execute();
    }

    public void execute(int stateType) {
        if (stateType == 1) {
            state = new NotPayState();
        } else if(stateType == 2) {
            state = new PayingState();
        } else if(stateType == 3) {
            state = new PaidState();
        }

        state.execute();
    }

}
