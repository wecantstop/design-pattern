package com.xuzhiguang.study.designpattern.state;

public class NotPayState implements State {
    public void execute() {
        System.out.println("执行订单未支付的逻辑");
    }
}
