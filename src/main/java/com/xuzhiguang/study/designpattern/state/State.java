package com.xuzhiguang.study.designpattern.state;

public interface State {

    void execute();

}
