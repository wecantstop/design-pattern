package com.xuzhiguang.study.designpattern.state;

public class PayingState implements State {
    public void execute() {
        System.out.println("执行订单支付中的逻辑");
    }
}
